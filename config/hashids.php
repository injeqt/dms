<?php

return [

    'salt' => env('HASHIDS_SALT', env('APP_KEY', 'MySecretSalt-Hugh'))

    /**
     * POTENTIAL COMBINATIONS
     * 62 ^ 4 = 14,776,336
     * 62 ^ 5 = 916,132,832
     * 62 ^ 6 = 56,800,235,584
     * 62 ^ 7 = 3,521,614,606,208
     **/
    ,'min_length' => env('HASHIDS_MIN_LENGTH', 4)

    //CHARS: 26*2+10 = 62
    ,'alphabet' => env('HASHIDS_ALPHABET', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')

];
