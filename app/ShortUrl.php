<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortUrl extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be guarded
     *
     * @var array
     */
    protected $guarded = ['id', 'updated_at'];


    public $appends = ['short_url'];


    /**
    * Get the appended $short_url attribute
    *
    * @return string
    */
    public function getShortUrlAttribute()
    {
        return ! empty($this->code)
            ? config('app.url') . '/' . $this->code
            : config('app.url') . '/' . hashids_encode($this->id);
    }

}
