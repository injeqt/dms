<?php

if( ! function_exists('dc') )
{
    /**
     * Dump data to screen and CONTINUE
     *
     * @param  $data
     * @return string
     */
    function dc($data)
    {
        return dump($data);
    }
}

if( ! function_exists('hashids') )
{
        function hashids($salt=null, $min_length=null, $alphabet=null)
        {
            $salt = $salt ? $salt : config('hashids.salt');
            $min_length = $min_length ? $min_length : config('hashids.min_length');
            $alphabet = $alphabet ? $alphabet : config('hashids.alphabet');
            return new Hashids\Hashids($salt, $min_length, $alphabet);
        }
}

if( ! function_exists('hashids_encode') )
{
        function hashids_encode($data)
        {
            $hashids = hashids();
            return $hashids->encode($data);
        }
}

if( ! function_exists('hashids_decode') )
{
        function hashids_decode($hash_code)
        {
            $hashids = hashids();
            return $hashids->decode($hash_code);
        }
}
