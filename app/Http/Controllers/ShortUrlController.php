<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateShortUrlRequest;

use App\ShortUrl;

class ShortUrlController extends Controller
{
    /**
     * Show the create Short URL page
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Store to the specified resource.
     *
     * @param  \App\Http\CreateShortUrlRequest $request
     * @return Redirect
     */
    public function store(CreateShortUrlRequest $request)
    {
        $short_url = ShortUrl::create(['url'=>$request->url]);

        $data = [ $short_url->id ];

        $code = hashids_encode($data);

        $short_url->code = $code;
        $short_url->save();

        return back()->with([
            'short_url' => $short_url
            /*,'success' => [
                'message' => 'Short URL successfully created.'
            ]*/
        ]);
    }

    /**
     * Redirect to the appropriate URL
     *
     * @param $hashcode
     * @return Redirect
     */
    public function redirect($hashcode)
    {
        //$original = $hashcode;

        if( $show_analytics = substr($hashcode, -1)=='+' ? true : false )
        {
            return $this->analytics($hashcode);
        }

        if( ! $data = hashids_decode($hashcode) )
        {
            return redirect('/')->with(['error' => ['message'=>'URL not found.']]);
        }

        $id = $data[0];

        if( ! $short_url = ShortUrl::find($id) )
        {
            return redirect('/')->with(['error' => ['message'=>'URL not found.']]);
        }

        $short_url->visits++;
        $short_url->save();

        return redirect($short_url->url);
    }

    /**
     * Display Short URL Analytics
     *
     * @param $hashcode
     * @return Response
     */
    protected function analytics($hashcode)
    {
        $hashcode = substr($hashcode, 0, strlen($hashcode) - 1);

        if( ! $data = hashids_decode($hashcode) )
        {
            return redirect('/')->with(['error' => ['message'=>'URL not found.']]);
        }

        $id = $data[0];

        if( ! $short_url = ShortUrl::find($id) )
        {
            return redirect('/')->with(['error' => ['message'=>'URL not found.']]);
        }

        return view('visits')->with(['short_url' => $short_url]);
    }

}
