@if( $message = Session::get('success.message') )
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif


@if( $message = Session::get('error.message') )
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif


@if( $message = Session::get('warning.message') )
<div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif


@if( $message = Session::get('info.message') )
<div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif


@if( $errors->any() )
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $errors->first() }}
</div>
@endif
