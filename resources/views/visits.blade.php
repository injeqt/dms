@extends('layouts.app')

<div class="container" style="padding:25px;">

    <h1>Analytics</h1>

    <div class="card" style="margin:0 auto;width:400px;padding:15px;background:#eee;">

        <div class="text-center" style="background:white;padding:25px 25px 0;">

            <div>SHORT URL</div>

            <h5>{{ $short_url->short_url }}</h5>

            <div style="margin-top:25px;">VISITS</div>
            <div style="margin-top:-20px;font-size:5em;">
                {{ number_format($short_url->visits, 0) }}
            </div>

            <div>REDIRECTS TO</div>
            <div style="margin-bottom:25px;"><small>{{ $short_url->url }}</small></div>

        </div>

    </div>

</div>
