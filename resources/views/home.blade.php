@extends('layouts.app')

<div class="container" style="padding:25px;">

    <h1>Quick URL Shortener</h1>

</div>

<div style="background:#eee;padding:20px 30px 10px;">

    <div class="container">

        <form action="/" method="POST">

            @csrf

            <div class="text-left mb-2 ml-3" style="color:#28a745;">Shorten Your Links</div>

            <div class="input-group input-group-lg">

                <input class="form-control{{ count($errors) ? ' is-invalid' : '' }}"
                    type="text"
                    ref="url"
                    name="url"
                    value="{{ old('url', '') }}"
                    placeholder="Your Original URL Here"
                    aria-label="Enter URL"
                    autofocus />

                <div class="input-group-append">
                    <button class="btn btn-success" type="submit"><i class="fa fa-plus fa-lg"></i></button>
                </div>

            </div>

            <div class="text-left mt-2 ml-3" style="color:#000;">
                <small>ie: http://yourdomain.com/some-long-name/even-more-words/never-ending</small>
            </div>

        </form>

    </div>

</div>

<div style="padding:20px 30px">

    <div class="container">@include('partials.alerts')</div>

    <div class="container">

        @if( $short_url = Session::get('short_url') )

        <div class="alert alert-success text-center">

            <div style="margin-top:5px;">YOUR SHORT URL:</div>

            <h5 style="margin-top:5px;margin-bottom:0;font-weight:500;">
                <i class="fa fa-globe fa-lg"></i>
                &nbsp;
                <a target="_shortened" href="{{ $short_url->short_url }}">{{ $short_url->short_url }}</a> &nbsp;&nbsp;
            </h5>

            <div><small>&rarr; {{ $short_url->url }}</small></div>

            <div style="margin-top:15px;">FOR STATS VISIT:</div>

            <h5>
                <i class="fa fa-chart-line fa-lg"></i>
                &nbsp;
                <a target="_analytics" href="{{ $short_url->short_url }}+">{{ $short_url->short_url }}+</a>
            </h5>

        </div>

        @endif

        <div class="text-center"><small>&copy; {{ date('Y') }}</small></div>

    </div>

</div>
