<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('short_urls', function (Blueprint $table)
        {
            $table->increments('id');

            // (26*2 + 10) ^ 5 = 62^5 = 916,132,832 --- 62^6 = 56,800,235,584
            $table->string('code', 5)->unique()->nullable();
            $table->text('url');
            $table->integer('visits')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('short_urls');
    }
}
